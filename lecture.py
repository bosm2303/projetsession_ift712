# Liste des fichiers et des librairies
import pandas as pd
import random


def fichier_entrainement(nom_fichier, pourcentage_test):
    # Lire l'ensemble du fichier
    donnees_fichier = pd.read_csv(nom_fichier)
    # Compter le nombre de lignes de donnees du fichier
    nombre_lignes_fichier = len(donnees_fichier)
    # Calculer le nombre de lignes selon le pourcentage
    nombre_lignes_test = int(pourcentage_test * nombre_lignes_fichier / 100)
    # Ordonner les numeros des lignes de donnees pour les donnees de test
    lignes_test = sorted(random.sample(list(range(nombre_lignes_fichier)),
                                       nombre_lignes_test))
    # Retourner les donnees du fichier et les numeros des lignes
    return donnees_fichier, lignes_test


def fichier_validation(nom_fichier):
    # Lire l'ensemble du fichier
    donnees_fichier = pd.read_csv(nom_fichier)
    # Retourner les donnees du fichier
    return donnees_fichier
