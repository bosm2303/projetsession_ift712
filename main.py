# IFT712 - Techniques d'apprentissage
# Projet de session
#
# Marc-Andre Bossanyi
# 15 081 180

# Liste des fichiers et des librairies
import sys
import numpy as np
import lecture
import gestion_donnees
import classification
import exportation_donnees

print("Debut du programme \n")

# 0. Lire les parametres en entree
print("Lecture des parametres \n")
liste_parametres = sys.argv
liste_parametres.pop(0)
parametre_m = int(liste_parametres.pop(0))
parametre_lambda = int(liste_parametres.pop(0))
liste_id_classification = (np.asarray(liste_parametres)).astype(int)

# 1. Lire le fichier pour entrainer
nom_fichier = "train.csv"
print("Lecture du fichier : " + nom_fichier)
pourcentage_test = 20
print(f"Pourcentage de donnees de test : {pourcentage_test}% \n")
donnees_fichier, numeros_lignes_test = lecture.fichier_entrainement(
    nom_fichier, pourcentage_test)

# 2. Obtenir les donnees d'entrainement
print("Obtenir les donnees d'entrainement")
donnees_entrainement = gestion_donnees.obtenir_donnees_entrainement(
    donnees_fichier, numeros_lignes_test)

# 3. Obtenir les cibles d'entrainement
print("Obtenir les cibles d'entrainement")
cibles_entrainement = gestion_donnees.obtenir_cibles_entrainement(
    donnees_fichier, numeros_lignes_test)

# 4. Obtenir les donnees de test
print("Obtenir les donnees de test")
donnees_test = gestion_donnees.obtenir_donnees_test(donnees_fichier,
                                                    numeros_lignes_test)

# 5. Obtenir les cibles de test
print("Obtenir les cibles de test \n")
cibles_test = gestion_donnees.obtenir_cibles_test(donnees_fichier,
                                                  numeros_lignes_test)

# 6. Lire le fichier pour valider
nom_fichier = "test.csv"
print("Lecture du fichier : " + nom_fichier + "\n")
donnees_fichier = lecture.fichier_validation(nom_fichier)

# 7. Obtenir les donnees de validation
print("Obtenir les donnees de validation \n")
donnees_validation = gestion_donnees.obtenir_donnees_validation(
    donnees_fichier)

# 8. Faire la validation croisee pour les classifications selon les
# identifiants en parametre
for id_classification in liste_id_classification:
    # Creer une variable de resultats contenant le parametre m, le parametre
    # lambda et l'erreur suite a une classification
    resultats = []
    # Creer variable pour le nom de la classification
    nom_classification = ""
    if id_classification == 1:
        nom_classification = "Ridge"
    elif id_classification == 2:
        nom_classification = "Logistique"
    elif id_classification == 3:
        nom_classification = "Perceptron"
    elif id_classification == 4:
        nom_classification = "SVM"
    elif id_classification == 5:
        nom_classification = "Descente de gradient stochastique"
    elif id_classification == 6:
        nom_classification = "Naif de Bayes gaussien"
    # Pour chaque valeur du parametre M
    for m in range(parametre_m):
        # Pour chaque valeur du parametre lambda
        for lambda_l in range(parametre_lambda):
            # Initialiser la valeur de lambda
            lambda_l = 1 / 2 ** lambda_l
            print(f"Entrainement : {nom_classification} avec m = {m} et " +
                  f"avec lambda = {lambda_l}")
            # Faire la classification sur les donnees d'entrainement
            erreur_test = classification.classifier_donnees_test(
                donnees_entrainement, cibles_entrainement,
                donnees_test, cibles_test, m, lambda_l,
                id_classification)
            print(f"Erreur de test = {erreur_test}% \n")
            # Enregistrer les parametres m, lambda et l'erreur
            resultats.append([m, lambda_l, erreur_test])

    # Trouver l'erreur minimum
    erreur_min = min([resultat[2] for resultat in resultats])
    # Trouver la position de l'erreur minimum
    position = [resultat[2] for resultat in resultats].index(erreur_min)
    # Trouver la meilleure paire (m, lambda) dans la liste de resultats
    meilleurs_parametres = resultats[position]
    print("Meilleure paire de parametres m et lambda est : " +
          f"[{meilleurs_parametres[0]}, {meilleurs_parametres[1]}] " +
          f"avec une erreur de test minimale de {meilleurs_parametres[2]}% \n")

    # Faire la classification sur les donnees de validation sachant les
    # meilleurs parametres, soit m et lambda
    print(f"Classification : {nom_classification} " +
          f"avec m = {meilleurs_parametres[0]} et " +
          f"avec lambda = {meilleurs_parametres[1]} \n")
    cibles_validation_predites = classification.classifier_donnees_validation(
        donnees_entrainement, cibles_entrainement,
        donnees_validation, meilleurs_parametres[0], meilleurs_parametres[1],
        id_classification)
    # Creer le fichier de sortie contenant la classification des feuilles
    # selon les cibles de validation predites
    print("Creation du fichier de sortie pour la classification : " +
          f"{nom_classification} \n \n")
    exportation_donnees.creer_fichier(cibles_validation_predites,
                                      id_classification)

print("Fin du programme")
