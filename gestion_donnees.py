# Liste des fichiers et des librairies
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler


def obtenir_donnees_entrainement(donnees_fichier, lignes_test):
    # Compter le nombre de lignes de donnees dans le fichier
    nombre_lignes_fichier = len(donnees_fichier)
    # Gener liste du numero des lignes de donnees pour l'entrainement
    lignes_entrainement = [lignes
                           for lignes in list(range(nombre_lignes_fichier))
                           if lignes not in lignes_test]
    # Ignorer les colonnes "id" et "species"
    donnees_filtrees = donnees_fichier.drop(["id", "species"],
                                            axis=1).values
    # Conserver les lignes de donnees de la liste des numeros
    donnees_entrainement_brutes = [donnees_filtrees[index]
                                   for index in lignes_entrainement]
    # Mettre a l'echelle les donnees
    echelonner = StandardScaler().fit(donnees_entrainement_brutes)
    donnees_entrainement = echelonner.transform(donnees_entrainement_brutes)
    # Retourner les donnees d'entrainement
    return donnees_entrainement


def obtenir_cibles_entrainement(donnees_fichier, lignes_test):
    # Compter le nombre de lignes de donnees dans le fichier
    nombre_lignes_fichier = len(donnees_fichier)
    # Gener liste du numero des lignes de donnees pour l'entrainement
    lignes_entrainement = [lignes
                           for lignes in list(range(nombre_lignes_fichier))
                           if lignes not in lignes_test]
    # Encoder les etiquettes par des nombres
    encodeur_etiquettes = LabelEncoder().fit(
        donnees_fichier["species"])
    cibles_entrainement = encodeur_etiquettes.transform(
        donnees_fichier["species"])
    # Conserver les lignes de donnees de la liste des numeros
    cibles_entrainement = [cibles_entrainement[index]
                           for index in lignes_entrainement]
    # Retourner les donnees cibles
    return np.asarray(cibles_entrainement)


def obtenir_donnees_test(donnees_fichier, lignes_test):
    # Ignorer les colonnes "id" et "species"
    donnees_filtrees = donnees_fichier.drop(["id", "species"],
                                            axis=1).values
    # Conserver les lignes de donnees de la liste des numeros
    donnees_test_brutes = [donnees_filtrees[index]
                           for index in lignes_test]
    # Mettre a l'echelle les donnees
    echelonner = StandardScaler().fit(donnees_test_brutes)
    donnees_test = echelonner.transform(donnees_test_brutes)
    # Retourner les donnees de test
    return donnees_test


def obtenir_cibles_test(donnees_fichier, lignes_test):
    # Encoder les etiquettes par des nombres
    encodeur_etiquettes = LabelEncoder().fit(
        donnees_fichier["species"])
    cibles_test = encodeur_etiquettes.transform(
        donnees_fichier["species"])
    # Conserver les lignes de donnees de la liste des numeros
    cibles_test = [cibles_test[index] for index in lignes_test]
    return np.asarray(cibles_test)


def obtenir_donnees_validation(donnees_fichier):
    # Ignorer la colonne "id"
    donnees_filtrees = donnees_fichier.drop(["id"], axis=1).values
    # Metter a l'echelle les donnees
    echelonner = StandardScaler().fit(donnees_filtrees)
    donnees_validation = echelonner.transform(donnees_filtrees)
    # Retourner les cibles de validation
    return donnees_validation
