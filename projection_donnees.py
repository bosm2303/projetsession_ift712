# Liste des fichiers et des librairies
from sklearn.preprocessing import PolynomialFeatures


def augmenter_dimension(donnees, dimensionalite):
    # Si la dimensionalite est zero, soit qu'on ne projette pas les
    # donnees dans une autre dimension
    if dimensionalite == 0:
        # Retourner les donnees initiales
        return donnees
    # Creer la fonction de base polynomial
    fonction_base = PolynomialFeatures(dimensionalite)
    # Obtenir les donnees augmentees dans une autre dimension
    donnees_augmentees = fonction_base.fit_transform(donnees)
    # Retourner les donnees augmentees
    return donnees_augmentees
