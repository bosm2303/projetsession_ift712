# Liste des fichiers et des librairies
import projection_donnees
from sklearn.linear_model import Ridge
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import GaussianNB


def classifier_donnees_test(donnees_entrainement, cibles_entrainement,
                            donnees_test, cibles_test, m, lambda_l,
                            id_classification):
    # Projeter les donnees d'entrainement et les donnees de test
    # dans une autre dimension
    donnees_entrainement_augmentees = projection_donnees.augmenter_dimension(
        donnees_entrainement, m)
    donnees_test_augmentees = projection_donnees.augmenter_dimension(
        donnees_test, m)
    # Entrainer le modele de classification avec les donnees d'entrainement
    # augmentees ainsi que les cibles d'entrainement selon
    # l'identifiant de la classification
    classifieur = entrainement(donnees_entrainement_augmentees,
                               cibles_entrainement, lambda_l,
                               id_classification)
    # Predire les cibles de test avec les donnees de test augmentees
    cibles_test_predites = prediction(classifieur, donnees_test_augmentees)
    # Calculer l'erreur de test avec les cibles de test predites et les
    # cibles reelles de test
    erreur_test = calcul_erreur(cibles_test, cibles_test_predites)
    # Retourner l'erreur de test
    return erreur_test


def classifier_donnees_validation(donnees_entrainement, cibles_entrainement,
                                  donnees_validation, m, lambda_l,
                                  id_classification):
    # Projeter les donnees d'entrainement et les donnees de validation
    # dans une autre dimension
    donnees_entrainement_augmentees = projection_donnees.augmenter_dimension(
        donnees_entrainement, m)
    donnees_validation_augmentees = projection_donnees.augmenter_dimension(
        donnees_validation, m)
    # Entrainer le modele de classification avec les donnees d'entrainement
    # augmentees ainsi que les cibles d'entrainement
    classifieur = entrainement(donnees_entrainement_augmentees,
                               cibles_entrainement, lambda_l,
                               id_classification)
    # Predire les cibles de validation avec les donnees de validation
    # augmentees
    cibles_validation_predites = prediction(classifieur,
                                            donnees_validation_augmentees)
    # Retourner les cibles predites de validation
    return cibles_validation_predites


def entrainement(donnees, cibles, lambda_l, id_classification):
    # Creer une variable pour la classifieur
    classifieur = None
    # Si l'identifiant de la classifiation est celui de Ridge
    if id_classification == 1:
        classifieur = Ridge(alpha=lambda_l**(-1),
                            solver="saga",
                            max_iter=10)
    elif id_classification == 2:
        classifieur = LogisticRegression(C=lambda_l,
                                         solver="saga",
                                         max_iter=10,
                                         multi_class="multinomial")
    elif id_classification == 3:
        classifieur = Perceptron(alpha=lambda_l,
                                 max_iter=10)
    elif id_classification == 4:
        classifieur = SVC(C=lambda_l,
                          max_iter=10)
    elif id_classification == 5:
        classifieur = SGDClassifier(alpha=lambda_l,
                                    max_iter=10)
    elif id_classification == 6:
        classifieur = GaussianNB(var_smoothing=lambda_l)
    # Entrainement du modele
    classifieur.fit(donnees, cibles)
    # Retourner le classifieur
    return classifieur


def prediction(classifieur, donnees):
    # Prediction des cibles avec le modele de classification
    cibles_predites = classifieur.predict(donnees)
    # Arrondir les cibles predites afin d'obtenir des entiers
    cibles_predites = cibles_predites.astype(int)
    # Retourner les cibles predites
    return cibles_predites


def calcul_erreur(cibles_reelles, cibles_predites):
    # Creer une liste contenant si une cible a ete bien predite, soit 0,
    # ou une cible n'a pas ete bien predite, soit 1
    tableau_erreur = []
    # Pour chaque indice de la liste des cibles predites
    for indice in range(len(cibles_predites)):
        # Si la cible predite est correcte
        if cibles_predites[indice] == cibles_reelles[indice]:
            # Ajouter 0 a la liste
            tableau_erreur.append(0)
        # Sinon, la cible predite est incorrecte
        else:
            tableau_erreur.append(1)
    # Calculer l'erreur
    erreur = (sum(tableau_erreur) / len(cibles_reelles)) * 100
    # Retourner l'erreur
    return erreur
