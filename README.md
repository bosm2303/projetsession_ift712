# ProjetSession_IFT712

## Introduction
Projet de session d'hiver 2019 dans le cadre du cours IFT712 - Techniques d'apprentissage de l'Université de Sherbrooke donné par M. Pierre-Marc Jodoin.

## Configurations du projet
* Python 3.7
* Librairies
	* Pandas
	* Numpy
	* Sklearn
		* Preprocessing
		* Linear_Model
		* SVM
		* Naive_Bayes
	* Random
	* Sys

## Lancement du projet
1. Ouvrir une fenêtre de commande (cmd) dans le répertoire où se trouve le fichier "main.py".
2. Écrire dans la fenêtre de commande (cmd) : *python -W ignore main.py «dimension» «lambda» «idClassification(s)»*.
3. Consulter les fichiers de sortie ".csv" dans le même répertoire où se trouve le fichier "main.py".

Notes : 
* *dimension* : Valeur maximale pour la projection des données dans une autre dimension, soit (D-1). 
**Attention** : Une dimension supérieure à 3 augmente le temps d'exécution !
* *lambda* : Valeur maximale pour le coefficient de la régularisation, soit (1 / 2)^lambda.
* *idClassification(s)* : Suite d'un ou plusieurs nombres associés à une classification, soit 
	* 1 - Ridge
	* 2 - Logistique 
	* 3 - Perceptron 
	* 4 - SVM 
	* 5 - Descente de gradient stochastique 
	* 6 - Naïf de Bayes gaussien
