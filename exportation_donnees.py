# Liste des fichiers et des librairies
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import lecture


def creer_fichier(cibles_validation, id_classification):
    # Lire le fichier des donnees d'entrainement
    nom_fichier = "train.csv"
    donnees_fichier_entrainement = lecture.fichier_validation(nom_fichier)
    # Chercher le nom des differentes especes de feuille
    encodeur_etiquettes = LabelEncoder().fit(
        donnees_fichier_entrainement["species"])
    # Compter le nombre d'especes de feuille
    nombre_especes = len(encodeur_etiquettes.classes_)
    # Lire le fichier des donnees de validation
    nom_fichier = "test.csv"
    donnees_fichier_validation = lecture.fichier_validation(nom_fichier)
    # Chercher les id des donnees du fichier de validation
    liste_id = donnees_fichier_validation.pop("id")
    # Creer un tableau permettant d'attribuer un id des cibles de
    # validation a une espece
    tableau_attribution = []
    for cible in cibles_validation:
        ligne = [0 if valeur != abs(cible)
                 else 1 for valeur in range(nombre_especes)]
        tableau_attribution.append(ligne)
    # Creer le fichier de sortie
    fichier_sortie = pd.DataFrame(tableau_attribution,
                                  index=liste_id,
                                  columns=encodeur_etiquettes.classes_)
    suffixe_nom_fichier = ""
    if id_classification == 1:
        suffixe_nom_fichier = "ridge"
    elif id_classification == 2:
        suffixe_nom_fichier = "logistique"
    elif id_classification == 3:
        suffixe_nom_fichier = "perceptron"
    elif id_classification == 4:
        suffixe_nom_fichier = "svm"
    elif id_classification == 5:
        suffixe_nom_fichier = "dgs"
    elif id_classification == 6:
        suffixe_nom_fichier = "nbg"
    fichier_sortie.to_csv("donnees_soumettre_" + suffixe_nom_fichier + ".csv")
